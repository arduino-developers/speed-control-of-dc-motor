// This code is for DC Motor controlled by L298N or L293D Motor Driver.
// Wiring Details
// ENA : 9 , IN1 : 8 & IN2 : 7
// This code is only for speed control, Please refer separate code available for direction control.
// OUT1 & OUT2 : Motor 1
// This code is only applicable for one motor, If needed please add code for second motor at your end.
// Please Join https://t.me/Arduinodeveloper for more updates and discussions about Arduino, Electronics, IoT and Embedded Systems

// Motor 1 Pins initialization
int ENA = 9;
int IN1 = 8;
int IN2 = 7;

void setup() {
  
  // Configuring Pins as outputs
  
  pinMode(ENA, OUTPUT);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  
  // Putting motors in off mode in starting
  
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, LOW);
  
}

void loop() {
  speedControl();
  delay(1000);
}

// This function for controling speed of the motors

void speedControl() {
// Turn on motor 1
  
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  delay(2000);
 
// Adjust motor speed as in maximum
   analogWrite(ENA,255);
   delay(2000);
  
 // Turn off motor 1
  
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, LOW);
  delay(2000);

// Adjust motor speed as in 50%  of full speed
    analogWrite(ENA,128);
    delay(2000);

// Turn off motor 1
  
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, LOW); 
  delay(2000);
}
